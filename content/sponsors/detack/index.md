---
title: "Detack GmbH"
weight: 4
resources:
- name: logo
  src: LOGO_DETACK_black_straight-02.png
---

The Detack GmbH provides food and drink for the CTFs we take part in.
