---
title: "About Us"
menu: "main"
---

The /upb/hack Team mainly is a student's initiative from the Paderborn University in Germany -- that formed from the urge to learn more about IT security. We meet regularly and participate in **C**apture **T**he **F**lag challenges, as they are a great way to learn more about security of IT systems -- but we are not exclucivly an CTF Team. We try to learn new stuff from every part of the IT security field.

#### Join us!

If you are interested in IT security and would like to find out if you'd like to join us, then the best way would be to come to one of our meetings. At out [Team page](/members/) you can have a look at the current team members. Normally we meet up once a week, you can find the next meeting's time, date, and place on our [Events page](/events/) under "Upcoming events". At the moment the meetings are held in German, but even if you do not speak German fluently: Don't hesitate, we will find a solution.

If you are concerned, that you are not experienced enough: That won't be a problem either. Every person in this team is in it to learn more about IT security, so join and get the experience you need, as we all do. Every team member has a different level of knowledge and experience, but that is what makes it so exiting -- we learn from each other.

#### What are CTFs?

"In computer security, Capture the Flag (CTF) is a computer security competition. CTF contests are usually designed to serve as an educational exercise to give participants experience in securing a machine, as well as conducting and reacting to the sort of attacks found in the real world. Reverse-engineering, network sniffing, protocol analysis, system administration, programming, and cryptanalysis are all skills which have been required by CTF contests. There are two main styles of capture the flag competitions: **attack/defense** and **Jeopardy!**.

In an **attack/defense** style competition, each team is given a machine (or a small network) to defend on an isolated network. Teams are scored on both their success in defending their assigned machine and on their success in attacking the other team's machines. Depending on the nature of the particular CTF game, teams may either be attempting to take an opponent's flag from their machine or teams may be attempting to plant their own flag on their opponent's machine.

**Jeopardy!**-style competitions usually involve multiple categories of problems, each of which contains a variety of questions of different point values and difficulties. Teams attempt to earn the most points in the competition's time frame (for example 24 hours), but do not directly attack each other. Rather than a race, this style of game play encourages taking time to approach challenges and prioritizes quantity of correct submissions over the timing."<div style="text-align: right"><a href=https://en.wikipedia.org/wiki/Capture_the_flag#Computer_security>from Wikipedia: CTF</a></div>